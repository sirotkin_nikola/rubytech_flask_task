1. ``python3 create_db.py`` - запустить для создания БД
2. ``flask db init`` - инициализация alembic
3. ``flask db migrate`` - создание миграций
4. ``flask db upgrade`` - применение миграций
5. ``celery -A app.celery worker -l INFO`` - запуск celery