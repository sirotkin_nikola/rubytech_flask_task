from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from .logger import setup_logger
from .celery_utils import make_celery

app = Flask(__name__)

# app.config['SECRET_KEY'] = 'your_secret_key'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CELERY_BROKER_CONNECTION_RETRY_ON_STARTUP'] = True

app.config['LOG_FILE'] = './logs/app.log'
logger = setup_logger(app.config['LOG_FILE'])

db = SQLAlchemy(app)
migrate = Migrate(app, db)

app.config.update(CELERY_CONFIG={
    'broker_url': 'redis://localhost:6379',
    'result_backend': 'redis://localhost:6379',
    'broker_connection_retry': True,
    'broker_connection_retry_on_startup': True,
})

celery = make_celery(app)


from app import routes

app.register_blueprint(routes.app_route)
