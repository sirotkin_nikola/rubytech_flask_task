from app import db
import uuid


class Resource(db.Model):
    __tablename__ = 'resource'
    id = db.Column(db.Integer, primary_key=True)
    uuid = db.Column(db.String(36), default=lambda: str(uuid.uuid4()), unique=True, nullable=False)
    url = db.Column(db.String(255), nullable=False)
    protocol = db.Column(db.String(10))
    domain = db.Column(db.String(100))
    domain_zone = db.Column(db.String(10))
    path = db.Column(db.String(100))
    parameters = db.Column(db.String(255))
    last_status_code = db.Column(db.Integer)
    availability = db.Column(db.Boolean)

    def repr(self):
        return f"Resource('{self.uuid}', '{self.url}')"