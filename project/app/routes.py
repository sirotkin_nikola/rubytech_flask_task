from flask import Blueprint, request
from . import logger
from .tasks import status_link, save_link_data
import re

app_route = Blueprint('routes', __name__)


@app_route.route('/api/send_link', methods=['POST'])
def handle_post_request():
    data = request.get_data(as_text=True)

    pattern = r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+'
    link = re.findall(pattern, data)

    if link:
        result_status = status_link.delay(data)
        result_status_code = result_status.wait()
        json_data = save_link_data.delay(data, result_status_code).wait()

        logger.info('%s - Статус код: %s', data, result_status_code)
        return json_data
    else:
        return f'{data} не является ссылкой'
