from . import celery, db, app
import requests
from .models import Resource
import json


@celery.task
def status_link(link):
    response = requests.get(link)
    status_code = response.status_code
    return status_code


@celery.task
def save_link_data(link, status, parameter=None):
    link_list = link.split('/')
    full_path = '/'.join(link_list[3:])
    path = full_path.split('/')[0]
    protocol = link_list[0].strip(':')
    domain = link_list[2]
    domain_zone = link_list[2].split('.')[1]
    parameters = full_path.split('?')
    if len(parameters) != 1:
        parameter = {}
        for i in parameters[1].split('&'):
            val_key = i.split('=')
            parameter[val_key[0]] = val_key[1]

    with app.app_context():
        resource = Resource(url=link, protocol=protocol, domain=domain,
                            domain_zone=domain_zone, path=path, parameters=str(parameter),
                            last_status_code=status, availability=False)
        db.session.add(resource)
        db.session.commit()
    data = {
            'url': link,
            'protocol': protocol,
            'domain': domain,
            'domain_zone': domain_zone,
            'path': path,
            'parameters': parameter if parameter else {},
            'last_status_code': status,
        }
    parsed_data = json.loads(json.dumps(data))

    return json.dumps(parsed_data, indent=4, ensure_ascii=False)
